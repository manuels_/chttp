#include <string.h>
#include <assert.h>
#include <openssl/ssl.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

#include "http.h"
#include "uri-parser.h"
#include "object.h"


#ifdef DEBUG
#define debugError(...) fprintf(stderr, __VA_ARGS__);
#define debugLog(...) fprintf(stdout, __VA_ARGS__);
#else
#define debugError(...)
#define debugLog(...)
#endif


#define READ_CHUNK_SIZE	4096
#define HTTPS 1
#define HTTPS_PORT "443"

#define HTTP 0
#define HTTP_PORT "80"

#define DEFAULT_METHOD "GET"
#define HTTP_VERSION "HTTP/1.1"
/**
 * Prepare headers for request
 * Take a JSON object as input and write it as a proper header in a string
 */

int prepareHeaders(JSONObject* headers, char* dest) {
    int destLen = strlen(dest);

    int appendToStr(const char* key, JSONData value, JSONObject object) {
        int headerLen = strlen(key) + 2 + strlen(value.string) + 2; // 2 => ": ", 2 => '\r\n'
        char* header = (char*) malloc(headerLen + 1);
        printf("%x %d\n", header, header);
        sprintf(header, "%s: %s\r\n", key, value.string); // "<key>: <value>"

        assert(NULL != realloc(dest, destLen + headerLen + 1));

        destLen += headerLen;
        strcat(dest, header);
        assert(header != NULL);
        printf("%x %d\n", header, header);
        printf("%d\n", header != NULL);
        free(header);
        printf("%d\n", header != NULL);
        return 0;
    }

    //JSONObjectForEach
    JSONObjectForEach(*headers, appendToStr);
    return 0;
}


/**
 * request format:
 * "<method> <path> <HTTP_VERSION> \r\n"
 * <headers>
 * \r\n
 * <body>
 */

int prepareRequest(HTTPRequest* request, URI* uri, char* dest) {
    // "<method> SP <path> SP <HTTP_VERSION> CRLF" (SP => " ", CRLF = "\r\n")
    char* method = request->method != NULL ? request->method : DEFAULT_METHOD;

    dest = (char*) malloc(strlen(method) + 1 + strlen(uri->path) + 1 + strlen(HTTP_VERSION) + 2 + 1); // 1 => SP, 2 => CRLF + NULL byte
    sprintf(dest, "%s%s %s %s\r\n", dest, method, uri->path, HTTP_VERSION);

    // headers
    prepareHeaders(request->requestHeaders, dest);

    //body
    assert(realloc(dest, strlen(dest) + 2 + strlen(request->requestBody) + 1) != NULL); // 2 => CRLF, 1 => NULL byte
    sprintf(dest, "%s\r\n%s", dest, request->requestBody);

    debugLog("[DEBUG] Request: \"%s\"", dest);
    return 0;
}


void freeUri(URI* uri) {
    if (uri->scheme != NULL) free(uri->scheme);
	if (uri->domain != NULL) free(uri->domain);
	if (uri->path != NULL) free(uri->path);
	if (uri->port != NULL) free(uri->port);
}

/**
 * Perform http/https request
 * set-up  request, open and close socket
*/
RequestExitCode request(HTTPRequest* request) {
    if (request->uriStr == NULL || !strcmp("", request->uriStr)) return InvalidURI;


    URI uri;
    short isHttps;
    char* req;
    int sockfd;

    struct addrinfo hints, *res;
    ParseURI(&uri, request->uriStr);

    if (uri.scheme == NULL)
        uri.scheme = "https";
    else if (strcmp(uri.scheme, "http") && strcmp(uri.scheme, "https")) {
        debugError("Invalid scheme : \"%s\"\n", uri.scheme);
        freeUri(&uri);
        return InvalidScheme;
    }
    isHttps = !strcmp(uri.scheme, "https");

    prepareRequest(request, &uri, req);


    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    if(getaddrinfo(uri.domain, uri.port != NULL ? uri.port : (isHttps ? HTTPS_PORT : HTTPS_PORT), &hints, &res)) {
        freeUri(&uri);
        free(req);
        debugError("Failed to connect to host\n")
        return ConnectionError; // TODO: maybe improve that to detect what exactly is the problem
    }


    if((sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) < 0) {
        freeUri(&uri);
        free(req);
        debugError("Failed to create socket\n");
        return SocketError;
    }

    if(connect(sockfd, res->ai_addr, res->ai_addrlen)) {
        close(sockfd);
        freeUri(&uri);
        free(req);
        debugError("Failed to connect to \"%s\" at port %s\n", uri.domain, isHttps ? HTTPS_PORT : HTTP_PORT);
        return ConnectionError;
    }

    int exitCode;
    if(isHttps)
        exitCode = httpsRequest(request, sockfd, req);
    else
        exitCode = httpRequest(request, sockfd, req);

    request->done = 1;
    close(sockfd);
    freeUri(&uri);
    free(req);



    return Done;
}

/**
 * Send request through socket and receive data
 */
RequestExitCode httpsRequest(HTTPRequest* request, int sockfd, char* req) {
        SSL *ssl;
        SSL_CTX *ssl_ctx;
        char buffer[READ_CHUNK_SIZE];
        char *temp = NULL,
             *pos;
        int size,
            outSize = 0;

        SSL_library_init();

        if((ssl_ctx = SSL_CTX_new(TLS_client_method())) == NULL) {
            debugError("Failed to create SSL_CTX structure\n");
            return SSLError;
        }

        if((ssl = SSL_new(ssl_ctx)) == NULL) {
            SSL_CTX_free(ssl_ctx);
            debugError("Failed to create SSL structure\n");
            return SSLError;
        }

        if(!SSL_set_fd(ssl, sockfd)) {
            SSL_CTX_free(ssl_ctx);
            SSL_shutdown(ssl);
            debugError("failed to set fd for the ssl\n");
            return SSLError;
        }

        if(SSL_connect(ssl) != 1) {
            SSL_CTX_free(ssl_ctx);
            SSL_shutdown(ssl);
            debugError("Failed handshake with host\n");
            return SSLHandshakeError;
        }

        if(SSL_write(ssl, req, strlen(req)+1) <= 0) {
            SSL_CTX_free(ssl_ctx);
            SSL_shutdown(ssl);
            debugError("Failed to send headers\n");
            return HTTPSendError;
        }


        while ((size = SSL_read(ssl, buffer, READ_CHUNK_SIZE)) > 0) {
            outSize += size-1;
            printf("%s\n\n\n", buffer);
            temp = (char*)realloc(temp, outSize+1); // TODO: check if returns NULL
            strncpy(temp + outSize-size, buffer, size-1);
        }
        if (size < 0) {
            debugError("Failed to receive data\n");
            return HTTPReceiveError;
        }

        temp[outSize] = 0;
        pos = strstr(temp, "\r\n\r\n");
        request->responseHeaders = (char*) malloc(pos-temp+3);
        request->responseBody = (char*) malloc(temp+outSize-pos+5);
        strncpy(request->responseHeaders, temp, pos-temp+2);
        request->responseHeaders[pos-temp+2] = 0;
        strncpy(request->responseBody, pos+4, temp+outSize-pos+4);
        request->responseBody[temp+outSize-pos+4] = 0;


        SSL_CTX_free(ssl_ctx);
        SSL_shutdown(ssl);
        return Done;
}


RequestExitCode httpRequest(HTTPRequest* request, int sockfd, char* req) {
    char buffer[READ_CHUNK_SIZE];
    char *temp = NULL,
         *pos;
    int size,
        outSize = 0;


    if(write(sockfd, req, strlen(req)+1) < 0) {
        debugError("Failed to send headers\n");
        return HTTPSendError;
    }


    while ((size = read(sockfd, buffer, READ_CHUNK_SIZE)) > 0) {
        outSize += size;
        temp = (char*)realloc(temp, outSize+1); // TODO: check if returns NULL
        assert(temp != NULL);
        strncpy(temp + outSize-size, buffer, size);
    }

    if (size < 0) {
        debugError("Failed to receive data\n");
        return HTTPReceiveError;
    }


    temp[outSize] = 0;
    pos = strstr(temp, "\r\n\r\n");
    request->responseHeaders = (char*) malloc(pos-temp+3);
    request->responseBody = (char*) malloc(temp+outSize-pos+5);
    strncpy(request->responseHeaders, temp, pos-temp+2);
    request->responseHeaders[pos-temp+2] = 0;
    strncpy(request->responseBody, pos+4, temp+outSize-pos+4);
    request->responseBody[temp+outSize-pos+4] = 0;

    free(temp);

    return Done;
}

#include <stdlib.h>
#include <string.h>

#include "uri-parser.h"


int ParseURI(URI *uri, const char *uriStr) {
	if (uri == NULL || uriStr == NULL) return 1;

	const char *pos, *p;
	int size;

	pos = strstr(uriStr, "://");

	if (pos == NULL) {
		uri->scheme = NULL;
	} else {
		size = pos-uriStr;
		uri->scheme = (char*)malloc(size);

		strncpy(uri->scheme, uriStr, size);
		uri->scheme[size] = 0;

		for (char *c = uri->scheme; *c; c++) {
			if ('A' <= *c && *c <= 'Z') *c += 'a' - 'A';
		}

		uriStr = pos + 3;
	}

	pos = strchr(uriStr, '/');
	p = strchr(uriStr, ':');

	if (p != NULL && p < pos) {
		size = pos != NULL ? pos - p - 1 : strlen(p+1);
		uri->port = (char*)malloc(size+1);

		strncpy(uri->port, p+1, size);
		uri->port[size] = 0;
	} else {
		uri->port = NULL;
		p = pos;
	}

	if (p == NULL) {
		size = strlen(uriStr);
		uri->domain = (char*)malloc(size+1);

		strcpy(uri->domain, uriStr);

		for (char *c = uri->domain; *c; c++) {
			if ('A' <= *c && *c <= 'Z') *c += 'a' - 'A';
		}

		uri->path = (char*)malloc(2);

		strcpy(uri->path, "/");
	} else {
		size = p-uriStr;
		uri->domain = (char*)malloc(size+1);

		strncpy(uri->domain, uriStr, size);
		uri->domain[size] = 0;
		for (char *c = uri->domain; *c; c++) {
			if ('A' <= *c && *c <= 'Z') *c += 'a' - 'A';
		}

		uriStr = pos;
		size = strlen(uriStr);
		uri->path = (char*)malloc(size+1);

		strcpy(uri->path, uriStr);
	}
	return 0;
}

LIB_NAME = libhttp

LIBS = ssl json
LIBS_DIR = ../cjson/build

SRC_DIR = src
OUT_DIR = build
OBJ_DIR = $(OUT_DIR)/obj
TESTS_DIR = tests
INCLUDES_DIR = include ../cjson/include

# variables for compiler
CC = gcc
LDLIBS = $(addprefix -l, $(LIBS))
LDFLAGS = $(addprefix -L, $(LIBS_DIR))
CFLAGS = $(addprefix -I, $(INCLUDES_DIR)) -DDEBUG -fPIC -g # -O3

SRCS = $(wildcard src/*.c)
OBJS = $(patsubst $(SRC_DIR)/%.c, $(OBJ_DIR)/%.o, $(SRCS))
TESTS = $(patsubst $(TESTS_DIR)/%.c, $(TESTS_DIR)/%, $(wildcard $(TESTS_DIR)/*.c))
SHARED = $(OUT_DIR)/$(LIB_NAME).so
STATIC = $(OUT_DIR)/$(LIB_NAME).a


.PHONY: obj all static shared test

all: shared static

static: $(STATIC)

shared: $(SHARED)

obj: $(OBJS)

test: $(TESTS)


$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c | $(OBJ_DIR)
	$(CC) $(CFLAGS) $(LDFLAGS) $(LDLIBS) -c -o $@ $<

$(SHARED): $(OBJS) | $(OUT_DIR)
	$(CC) $(CFLAGS) $(LDFLAGS) $(LDLIBS) -shared -o $@ $(OBJS)

$(STATIC): $(OBJS) | $(OUT_DIR)
	ar rcs $@ $(OBJS)

$(TESTS_DIR)/%: $(TESTS_DIR)/%.c $(SHARED) | $(TESTS_DIR)
	$(CC) $< -o $@ $(CFLAGS) $(LDFLAGS) $(LDLIBS) -L$(OUT_DIR) $(patsubst $(OUT_DIR)/lib%.so, -l%, $(SHARED)) -g -Wl,-rpath='$${ORIGIN}/../build/'


$(OUT_DIR):
	mkdir -p $(OUT_DIR)
$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)
$(TESTS_DIR):
	mkdir -p $(TESTS_DIR)


.PHONY: clean
clean:
	rm -rf build/*

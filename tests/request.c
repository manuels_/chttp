#include <stdio.h>
#include "http.h"
#include "object.h"


int main() {
    HTTPRequest req;
    JSONObject headers;

    JSONObjectInit(&headers, JOT_HASH);
    JSONObjectSet(headers, "Host", (JSONData){.type=JT_STRING, .string="archlinux.org"});
    JSONObjectSet(headers, "Connection", (JSONData){.type=JT_STRING, .string="close"});

    req.uriStr = "https://archlinux.org";
    req.method = "GET";
    req.requestHeaders = &headers;// "Accept-Encoding: gzip\r\n";
    req.requestBody = NULL;
    req.responseHeaders = NULL;
    req.responseBody = NULL;
    request(&req);
    printf("Headers: \"%s\"\n\n", req.responseHeaders);
/*
    int i = 0;
    while(req.responseBody[i]) {
        printf("%d ", req.responseBody[i]);
        i++;
    }*/
    printf("Body: \"%s\"\n", req.responseBody);

    JSONObjectClean(headers);
    return 0;
}

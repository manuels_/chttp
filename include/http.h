#include "object.h"

#ifndef HTTP_H
#define HTTP_H


#ifdef __cplusplus
extern "C" {
#endif // __cplusplus


typedef struct HTTPRequest {
	char *uriStr;
	JSONObject *requestHeaders;
	char *requestBody;
	char *method;
	short done; // request performed
	short statusCode;
	char *responseBody;
	char *responseHeaders;
} HTTPRequest;



typedef enum {
	Done,
	InvalidURI,
	InvalidScheme,
	DomainNotFound,
	SocketError,
	ConnectionError,
	SSLError,
	SSLHandshakeError,
	HTTPSendError,
	HTTPReceiveError
	// other errors such as ssl, host not found, ...
} RequestExitCode;


RequestExitCode request(HTTPRequest* request);
RequestExitCode httpsRequest(HTTPRequest* request, int sockfd, char* req);
RequestExitCode httpRequest(HTTPRequest* request, int sockfd, char* req);


#ifdef __cplusplus
}
#endif // __cplusplus


#endif // !HTTP_H

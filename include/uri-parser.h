#ifndef URI_PARSER_H
#define URI_PARSER_H

typedef struct URI {
	char *scheme;
	char *domain;
	char *port;
	char *path;
} URI;

int ParseURI(URI *uri, const char *uriStr);

#endif // !URI_PARSER_H
